import db from '@/utils/db'
import { DataTypes } from 'sequelize'
import { Corresponding, CorrespondingCreation } from '#/corresponding'

const corresponding = db.define<Corresponding, CorrespondingCreation>(
  'Corresponding',
  {
    electricity: DataTypes.FLOAT,
    gas: DataTypes.FLOAT,
    waterElec: DataTypes.FLOAT,
    waterCO2: DataTypes.FLOAT,
    endAt: {
      type: DataTypes.DATE,
      allowNull: true,
    },
  },
  {
    updatedAt: false,
  }
)

export default corresponding
