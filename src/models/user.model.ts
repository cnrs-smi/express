import db from '@/utils/db'
import { DataTypes } from 'sequelize'
import { User, UserCreation } from '#/user'
import bcrypt from 'bcrypt'
import Typology from '@/models/typology.model'

const user = db.define<User, UserCreation>(
  'User',
  {
    primaryResidence: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        is: '^ind|col|other$',
      },
    },
    surface: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    peopleNumber: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        isEmail: true,
      },
      unique: true,
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    emailConfirmed: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
    },
    emailConfirmationToken: DataTypes.STRING,
    isAdmin: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
    },
  },
  {
    hooks: {
      beforeCreate: async (user: User): Promise<void> => {
        user.password = bcrypt.hashSync(user.password as string, await bcrypt.genSalt(10, 'a'))
      },
      beforeUpdate: async (user: User): Promise<void> => {
        if (user.changed('password')) {
          user.password = bcrypt.hashSync(user.password as string, await bcrypt.genSalt(10, 'a'))
        }
      },
    },
  },
)

user.belongsTo(Typology, {
  foreignKey: 'CodeINSEE',
})

export default user
