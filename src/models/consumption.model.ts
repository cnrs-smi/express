import db from '@/utils/db'
import { Consumption, ConsumptionCreation } from '#/consumption'
import { DataTypes } from 'sequelize'
import User from '@/models/user.model'

const consumption = db.define<Consumption, ConsumptionCreation>(
  'Consumption',
  {
    electricity: DataTypes.INTEGER,
    gas: DataTypes.INTEGER,
    water: DataTypes.INTEGER,
  },
)

consumption.belongsTo(User, {
  foreignKey: 'consumer',
})

export default consumption
