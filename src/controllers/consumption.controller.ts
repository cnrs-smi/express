import { Router } from 'express'
import jwt from 'jsonwebtoken'
import { JWTBody, ListMeta } from '#/index'
import User from '@/models/user.model'
import authorizeCheck from '@/utils/authorizeCheck'
import {
  BadRequestException,
  ForbiddenException,
  NotFoundException,
  ServerErrorException,
  UnauthorizedException,
} from '@/utils/exceptions'
import Consumption from '@/models/consumption.model'
import type { Attributes, FindOptions, Model } from 'sequelize'
import dayjs from 'dayjs'
import Corresponding from '@/models/corresponding.model'

const ConsumptionController = Router()

ConsumptionController.use(authorizeCheck)

ConsumptionController.post('/', async (req, res) => {
  try {
    const { id } = jwt.decode(req.headers['authorization']?.split('Bearer ')[1] as string) as JWTBody

    const user = await User.findByPk(id, { attributes: ['id'] })
    if (!user) {
      return res
        .status(401)
        .json(new UnauthorizedException(
          'Aucun utilisateur trouvé avec cet identifiant',
          'user.not.found',
        ))
    }

    const consumption = await Consumption.create({ ...req.body, consumer: user.id }, { include: [User] })

    return res
      .status(201)
      .json(consumption)
  } catch (err) {
    return res
      .status(500)
      .json(new ServerErrorException(
        (err as Error).message,
        'server.error',
      ))
  }
})

ConsumptionController.get('/', async (req, res) => {
  try {
    const { id } = jwt.decode(req.headers['authorization']?.split('Bearer ')[1] as string) as JWTBody

    const options: FindOptions<Attributes<Model>> = {
      where: { consumer: id },
      order: [['createdAt', 'DESC']],
    }

    const { limit, page } = req.query

    if (limit && page) {
      options.limit = parseInt(limit as string)
      options.offset = (parseInt(page as string) - 1) * options.limit
    }

    const corresponding = (await Corresponding.findAll({ order: [['createdAt', 'DESC']] })).map((corresponding) => {
      const { createdAt, endAt, ...rest } = corresponding.toJSON()
      return {
        ...rest,
        createdAt: dayjs(createdAt),
        endAt: endAt ? dayjs(endAt) : null,
      }
    })
    const consumptions = (await Consumption.findAll(options)).map((consumption) => {
      const updateDate = dayjs(consumption.updatedAt)
      const correspondingLinked = corresponding.find((corresponding) => {
        const { createdAt, endAt } = corresponding
        return updateDate.isSameOrAfter(createdAt) && updateDate.isBefore(endAt || dayjs())
      })

      const electricityCO2 = correspondingLinked?.electricity && consumption.electricity
        ? (consumption.electricity * correspondingLinked.electricity).toFixed(3)
        : 'N/A'
      const gasCO2 = correspondingLinked?.gas && consumption.gas
        ? (consumption.gas * correspondingLinked.gas).toFixed(3)
        : 'N/A'
      const waterCO2 = consumption.water && correspondingLinked?.waterElec && correspondingLinked?.waterCO2
        ? ((consumption.water * correspondingLinked.waterElec) / correspondingLinked.waterCO2).toFixed(3)
        : 'N/A'
      return {
        ...consumption.toJSON(),
        electricityCO2,
        gasCO2,
        waterCO2,
      }
    })
    const meta: ListMeta = {}
    if (limit && page) {
      meta.count = await Consumption.count({ where: { consumer: id } })
      meta.limit = options.limit
      meta.page = parseInt(page as string)
    }

    return res
      .status(200)
      .json({
        data: consumptions,
        meta,
      })
  } catch (err) {
    return res
      .status(500)
      .json(new ServerErrorException(
        (err as Error).message,
        'server.error',
      ))
  }
})

ConsumptionController.patch('/:id', async (req, res) => {
  try {
    const consumptionId = req.params.id

    const consumption = await Consumption.findByPk(consumptionId)
    if (!consumption) {
      return res
        .status(404)
        .json(new NotFoundException(
          'Entrée introuvable',
          'consumption.not.found',
        ))
    }

    const { id: userId } = jwt.decode(req.headers['authorization']?.split('Bearer ')[1] as string) as JWTBody
    if (userId !== consumption.consumer) {
      return res
        .status(403)
        .json(new ForbiddenException(
          'Vous n\'avez pas les droits pour modifier cette entrée',
          'consumption.not.authorized',
        ))
    }

    if (!req.body.electricity && !req.body.water && !req.body.gas) {
      return res
        .status(400)
        .json(new BadRequestException(
          'Vous devez fournir au moins une valeur',
          'consumption.missing.value',
        ))
    }

    return res
      .status(200)
      .json(await consumption.update({
        electricity: req.body.electricity || consumption.electricity,
        water: req.body.water || consumption.water,
        gas: req.body.gas || consumption.gas,
      }))
  } catch (err) {
    return res
      .status(500)
      .json(new ServerErrorException(
        (err as Error).message,
        'server.error',
      ))
  }

})

export default ConsumptionController
