import { Router } from 'express'
import User from '@/models/user.model'
import { BadRequestException, NotFoundException, ServerErrorException } from '@/utils/exceptions'
import authorizeCheck from '@/utils/authorizeCheck'
import { Op } from 'sequelize'

const UsersController = Router()

UsersController.patch('/:id', async (req, res) => {
  try {
    const id = Number(req.params.id)

    if (!Number.isInteger(id)) {
      return res
        .status(400)
        .json(new BadRequestException(
          'L\'ID doit être un nombre entier',
          'user.id.bad.format',
        ))
    }

    let user = await User.findByPk(id)
    if (!user) {
      return res
        .status(404)
        .json(new NotFoundException(
          'Utilisateur introuvable',
          'user.not.found',
        ))
    }

    user = await user.update(req.body)
    return res
      .status(200)
      .json(user)
  } catch (err) {
    return res
      .status(500)
      .json(new ServerErrorException(
        (err as Error).message,
        'server.error',
      ))
  }
})

UsersController.delete('/:id', async (req, res) => {
  try {
    const id = Number(req.params.id)

    if (!Number.isInteger(id)) {
      return res
        .status(400)
        .json(new BadRequestException(
          'L\'ID doit être un nombre entier',
          'user.id.bad.format',
        ))
    }

    const user = await User.findByPk(id)
    if (!user) {
      return res
        .status(404)
        .json(new NotFoundException(
          'Utilisateur introuvable',
          'user.not.found',
        ))
    }

    await user.destroy()
    return res
      .status(204)
      .json()
  } catch (err) {
    return res
      .status(500)
      .json(new ServerErrorException(
        (err as Error).message,
        'server.error',
      ))
  }
})

UsersController.use(authorizeCheck)

UsersController.get('/:email', async (req, res) => {
  try {
    const email: string = req.params.email

    const user = await User.findAll({
      where: {
        email: {
          [Op.like]: `%${email}%`,
        }
      },
      attributes: {
        exclude: ['emailConfirmationToken', 'password'],
      }
    })
    if (!user) {
      return res
        .status(404)
        .json(new NotFoundException(
          'Utilisateur introuvable',
          'user.not.found',
        ))
    }

    return res
      .status(200)
      .json(user)
  } catch (err) {
    return res
      .status(500)
      .json(new ServerErrorException(
        (err as Error).message,
        'server.error',
      ))
  }
})

export default UsersController
