import { Router } from 'express'
import { BadRequestException, NotFoundException, ServerErrorException } from '@/utils/exceptions'
import Typology from '@/models/typology.model'

const TypologyController = Router()

TypologyController.get('/:codeINSEE', async (req, res) => {
  try {
    const codeINSEE = Number(req.params.codeINSEE)

    if (!Number.isInteger(codeINSEE)) {
      return res
        .status(400)
        .json(new BadRequestException(
          'Le code INSEE fourni n\'a pas le bon format.',
          'typology.bad.format',
        ))
    }

    const typology = await Typology.findByPk(codeINSEE, { attributes: ['CodeINSEE', 'Type_de_territoires'] })
    if (!typology) {
      return res
        .status(404)
        .json(new NotFoundException(
          'Aucune typologie de logement trouvée avec ce code INSEE.',
          'typology.not.found',
        ))
    }

    return res
      .status(200)
      .json(typology)
  } catch (err) {
    return res
      .status(500)
      .json(new ServerErrorException(
        (err as Error).message,
        'server.error',
      ))
  }
})

export default TypologyController
