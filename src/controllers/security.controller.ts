import { Router } from 'express'
import User from '@/models/user.model'
import {
  BadRequestException,
  ConflictRequestException,
  NotFoundException,
  ServerErrorException,
  UnauthorizedException,
} from '@/utils/exceptions'
import bcrypt from 'bcrypt'
import jwt, { JwtPayload } from 'jsonwebtoken'
import mail from '@/utils/mail'
import Typology from '@/models/typology.model'
import env from '@/utils/env'
import emailConfirmation, { createEmailConfirmationToken } from '@/utils/emails/emailConfirmation'
import emailConfirmed from '@/utils/emails/emailConfirmed'
import { JWTBody } from '#/index'
import authorizeCheck from '@/utils/authorizeCheck'

const SecurityController = Router()

SecurityController.post('/register', async (req, res) => {
  try {
    const email = req.body.email
    let user = await User.findOne({ where: { email } })
    if (user) {
      return res
        .status(409)
        .json(new ConflictRequestException(
          'Cet email est déjà utilisé',
          'email.already.used',
        ))
    }

    const typology = await Typology.findOne({
      where: { CodeINSEE: req.body.CodeINSEE },
    })
    if (!typology) {
      return res
        .status(404)
        .json(new NotFoundException(
          'Ce code INSEE n\'existe pas',
          'typology.not.found',
        ))
    }

    const token = createEmailConfirmationToken(email)
    user = await User.create({ ...req.body, emailConfirmationToken: token }, { include: [Typology] })
    mail.sendMail(emailConfirmation(user, token))

    return res
      .status(201)
      .json(user)
  } catch (err) {
    return res
      .status(500)
      .json(new ServerErrorException(
        (err as Error).message,
        'server.error',
      ))
  }
})

SecurityController.post('/email-check', async (req, res) => {
  try {
    const token = req.body.token
    if (jwt.verify(token, env('EMAIL_SECRET', 'salt1') as string)) {
      const email = (jwt.decode(token) as JwtPayload | null)?.email
      const exp = (jwt.decode(token) as JwtPayload | null)?.exp

      if (email && exp) {
        const user = await User.findOne({ where: { email } })
        if (user) {
          if (user.emailConfirmed) {
            return res
              .status(400)
              .json(new BadRequestException(
                'Le compte a déjà été confirmé',
                'email.already.confirmed',
              ))
          }

          if (user.emailConfirmationToken === token) {
            if ((Date.now() / 1000) < exp) {
              await user.update({ emailConfirmed: true, emailConfirmationToken: null })
              mail.sendMail(emailConfirmed(user))
              return res
                .status(200)
                .json(user)
            }

            return res
              .status(400)
              .json(new BadRequestException(
                'Le token est expiré',
                'token.expired',
              ))
          }

          return res
            .status(400)
            .json(new BadRequestException(
              'Token invalide',
              'token.invalid',
            ))
        }

        return res
          .status(400)
          .json(new BadRequestException(
            'Le token envoyé contient des informations invalides',
            'token.information.invalid',
          ))
      }
    }

    return res
      .status(400)
      .json(new BadRequestException(
        'Token invalide',
        'token.invalid',
      ))
  } catch (err) {
    return res
      .status(500)
      .json(new ServerErrorException(
        (err as Error).message,
        'server.error',
      ))
  }
})

SecurityController.post('/email-resent', async (req, res) => {
  try {
    const email = req.body.email

    if (!email) {
      return res
        .status(400)
        .json(new BadRequestException(
          'L\'email est requis',
          'email.required',
        ))
    }

    const user = await User.findOne({ where: { email } })
    if (user && !user.emailConfirmed) {
      const token = createEmailConfirmationToken(email)
      await user.update({ emailConfirmationToken: token })
      mail.sendMail(emailConfirmation(user, token))
      return res
        .status(200)
        .json({ message: 'Si votre adresse email est correcte, un nouveau mail de confirmation vous a été envoyé' })
    }

    return res
      .status(200)
      .json({ message: 'Si votre adresse email est correcte, un nouveau mail de confirmation vous a été envoyé' })
  } catch (err) {
    return res
      .status(500)
      .json(new ServerErrorException(
        (err as Error).message,
        'server.error',
      ))
  }
})

SecurityController.post('/login', async (req, res) => {
  try {
    const email = req.body.email

    if (!email) {
      return res
        .status(400)
        .json(new BadRequestException(
          'L\'email est requis',
          'email.required',
        ))
    }

    const user = await User.findOne({
      where: { email },
      attributes: {
        exclude: ['emailConfirmationToken'],
      }
    })
    if (!user) {
      return res
        .status(401)
        .json(new BadRequestException(
          'Les identifiants sont incorrects',
          'credentials.invalid',
        ))
    }

    if (!user.emailConfirmed) {
      return res
        .status(400)
        .json(new BadRequestException(
          'Vous devez confirmer votre adresse email',
          'email.not.confirmed',
        ))
    }

    if (bcrypt.compareSync(req.body.password, user.password as string)) {
      const { password, ...userWithoutPassword } = user.toJSON()
      return res
        .status(200)
        .json({
          user: userWithoutPassword,
          jwt: jwt.sign(
            {
              id: user.id,
              email: user.email,
              exp: Math.floor(Date.now() / 1000) + (60 * 60 * 24),
            },
            env('LOGIN_SECRET', 'salt2') as string,
          ),
        })
    }

    return res
      .status(401)
      .json(new BadRequestException(
        'Les identifiants sont incorrects',
        'credentials.invalid',
      ))
  } catch (err) {
    return res
      .status(500)
      .json(new ServerErrorException(
        (err as Error).message,
        'server.error',
      ))
  }
})

SecurityController.use(authorizeCheck)

SecurityController.get('/me', async (req, res) => {
  try {
    const { id: myId } = jwt.decode(req.headers['authorization']?.split('Bearer ')[1] as string) as JWTBody

    const me = await User.findByPk(myId, {
      attributes: {
        exclude: ['password', 'emailConfirmationToken'],
      }
    })
    if (!me) {
      return res
        .status(401)
        .json(new UnauthorizedException(
          'Le JWT fourni n\'est pas valide.',
          'jwt.not.valid',
        ))
    }

    return res
      .status(200)
      .json(me)
  } catch (err) {
    return res
      .status(500)
      .json(new ServerErrorException(
        (err as Error).message,
        'server.error',
      ))
  }
})

export default SecurityController
