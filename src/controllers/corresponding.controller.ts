import { Router } from 'express'
import Corresponding from '@/models/corresponding.model'
import { ServerErrorException } from '@/utils/exceptions'
import authorizeCheck from '@/utils/authorizeCheck'

const CorrespondingController = Router()

CorrespondingController.use(authorizeCheck)

CorrespondingController.get('/', async (req, res) => {
  try {
    return res
      .status(200)
      .json(await Corresponding.findAll({ order: [['createdAt', 'DESC']] }))
  } catch (err) {
    return res
      .status(500)
      .json(new ServerErrorException(
        (err as Error).message,
        'server.error',
      ))
  }
})

CorrespondingController.post('/', async (req, res) => {
  try {
    const lastCorresponding = await Corresponding.findOne({
      order: [['createdAt', 'DESC']],
    })
    const newCorresponding = await Corresponding.create(req.body)
    await Corresponding.update({ endAt: newCorresponding.createdAt }, { where: { id: lastCorresponding?.id } })

    return res
      .status(201)
      .json(newCorresponding)
  } catch (err) {
    return res
      .status(500)
      .json(new ServerErrorException(
        (err as Error).message,
        'server.error',
      ))
  }
})

export default CorrespondingController
