import { ApiException, ExceptionMessageType, HttpStatusCode } from '#/exceptions'

class Exception implements ApiException {
  constructor(
    readonly message: ExceptionMessageType,
    readonly status: HttpStatusCode,
    readonly codeError: string,
  ) {
  }
}

export class NotFoundException extends Exception {
  constructor(message: ExceptionMessageType, codeError: string) {
    super(message, 404, codeError)
  }
}

export class BadRequestException extends Exception {
  constructor(message: ExceptionMessageType, codeError: string) {
    super(message, 400, codeError)
  }
}

export class UnauthorizedException extends Exception {
  constructor(message: ExceptionMessageType, codeError: string) {
    super(message, 401, codeError)
  }
}

export class ForbiddenException extends Exception {
  constructor(message: ExceptionMessageType, codeError: string) {
    super(message, 403, codeError)
  }
}

export class ConflictRequestException extends Exception {
  constructor(message: ExceptionMessageType, codeError: string) {
    super(message, 409, codeError)
  }
}

export class ServerErrorException extends Exception {
  constructor(message: ExceptionMessageType, codeError: string) {
    super(message, 500, codeError)
  }
}
