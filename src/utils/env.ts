export default (
  envConstName: string,
  defaultValue: string | number,
  isNumber = false,
): string | number => {
  if (isNumber) {
    return parseInt(process.env[`JJTQ_${envConstName}`] || '', 10) || defaultValue as number
  }

  return process.env[`JJTQ_${envConstName}`] || defaultValue
}
