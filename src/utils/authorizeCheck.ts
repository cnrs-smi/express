import { NextFunction, Request, Response } from 'express'
import jwt from 'jsonwebtoken'
import { JWTBody } from '#/index'
import { UnauthorizedException } from '@/utils/exceptions'
import env from '@/utils/env'

export default (req: Request, res: Response, next: NextFunction) => {
  const jwtHeader = req.headers['authorization']?.split('Bearer ')[1]

  if (!jwtHeader) {
    return res
      .status(403)
      .json(new UnauthorizedException(
        'Vous devez être connecté pour accéder à cette ressource.',
        'jwt.not.found',
      ))
  }

  if (!jwt.verify(jwtHeader, env('LOGIN_SECRET', 'salt2') as string)) {
    return res
      .status(401)
      .json(new UnauthorizedException(
        'Le JWT fourni n\'est pas valide.',
        'jwt.not.valid',
      ))
  }

  const { email, id, iat, exp } = jwt.decode(jwtHeader) as JWTBody
  const now = Date.now() / 1000 - 10

  if (!email && !id && !iat && !exp) {
    return res
      .status(401)
      .json(new UnauthorizedException(
        'Le JWT fourni n\'est pas valide.',
        'jwt.not.valid',
      ))
  }

  if (exp < now) {
    return res
      .status(401)
      .json(new UnauthorizedException(
        'Votre session a expiré.',
        'session.expired',
      ))
  }

  next()
}
