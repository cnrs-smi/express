import nodemailer from 'nodemailer'
import env from '@/utils/env'

export default nodemailer.createTransport({
  host: env('MAIL_HOST', 'smtp.hostname.com') as string,
  port: env('MAIL_PORT', 587, true) as number,
  secure: true,
  auth: {
    user: env('MAIL_USER', 'username') as string,
    pass: env('MAIL_PASSWORD', 'password') as string,
  },
  tls: { rejectUnauthorized: false },
})
