import dotenv from 'dotenv'
import path from 'path'

dotenv.config({
  path: path.resolve(__dirname, '../../.env.local'),
})

import UserModel from '@/models/user.model'
import TypologyModel from '@/models/typology.model'
import ConsumptionModel from '@/models/consumption.model'
import CorrespondingModel from '@/models/corresponding.model'

(async () => {
  await TypologyModel.sync({ alter: true })
  await UserModel.sync({ alter: true })
  await ConsumptionModel.sync({ alter: true })
  await CorrespondingModel.sync({ alter: true })
})()
