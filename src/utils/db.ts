import type { Dialect } from 'sequelize'
import { Sequelize } from 'sequelize'
import env from '@/utils/env'

export default new Sequelize(
  env('DB_NAME', 'postgres') as string,
  env('DB_USER', 'root') as string,
  env('DB_PASSWORD', 'root') as string,
  {
    host: env('DB_HOST', 'localhost') as string,
    dialect: env('DB_TYPE', 'postgres') as Dialect,
  }
)

