import { User } from '#/user'
import env from '@/utils/env'

export default (user: User) => ({
  from: 'jmarco@jojotique.fr',
  to: user.email,
  subject: 'Email validé',
  html: `
<h1>Bienvenue !</h1>
<p>Merci d'avoir validé votre adresse mail. Maintenant vous pouvez
<a href="${env('FRONT_URL', 'http://localhost:3000')}/login">vous connecter</a>
.
</p>
`,
})
