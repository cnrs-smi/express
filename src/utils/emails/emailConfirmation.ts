import { User } from '#/user'
import env from '@/utils/env'
import jwt from 'jsonwebtoken'

export const createEmailConfirmationToken = (email: string): string => {
  return jwt.sign({
      email,
      exp: Math.floor(Date.now() / 1000) + (60 * 60 * 24),
    },
    env('EMAIL_SECRET', 'salt1') as string,
  )
}

export default (user: User, token: string) => ({
  from: 'jmarco@jojotique.fr',
  to: user.email,
  subject: 'Création de compte',
  html: `
<h1>Bonjour</h1>
<p>Merci de nous rejoindre. Pour valider votre compte merci de cliquer sur ce lien :
<a href="${env('EMAIL_CONFIRMATION', 'http://localhost:3000')}?token=${token}">${env('EMAIL_CONFIRMATION', 'http://localhost:3000')}?token=${token}</a> 
</p>
`,
})
