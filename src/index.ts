import dotenv from 'dotenv'
import path from 'path'

dotenv.config({
  path: path.resolve(__dirname, '../.env.local'),
})

import cors from 'cors'
import express from 'express'
import { ExceptionsHandler } from '@/middlewares/exceptions.handler'
import { UnknownRoutesHandler } from '@/middlewares/unknownRoutes.handler'
import env from '@/utils/env'
import UsersController from '@/controllers/users.controller'
import SecurityController from '@/controllers/security.controller'
import TypologyController from '@/controllers/typology.controller'
import ConsumptionController from '@/controllers/consumption.controller'
import CorrespondingController from '@/controllers/corresponding.controller'

import * as dayjs from 'dayjs'
import isSameOrAfter from 'dayjs/plugin/isSameOrAfter'
dayjs.extend(isSameOrAfter)

const app = express()
app.use(express.json())
app.use(cors({
  origin: env('FRONT_URL', 'http://localhost:3000') as string,
}))

app.use('/api/users', UsersController)
app.use('/api/typologies', TypologyController)
app.use('/api/consumptions', ConsumptionController)
app.use('/api/corresponding', CorrespondingController)
app.use('/', SecurityController)

app.get('/', (req, res) => res.send(JSON.stringify({ message: 'Hello world!' })))

app.all('*', UnknownRoutesHandler)

app.use(ExceptionsHandler)

app.listen(
  env('APP_PORT', 1337, true) as number,
  env('APP_HOST', 'localhost') as string,
  () => console.log('Silence, ça tourne.'),
)
