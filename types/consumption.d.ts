import { Model } from 'sequelize'

export interface ConsumptionCreation {
  electricity: number;
  gas: number;
  water: number;
}

export interface Consumption extends Model<Consumption, ConsumptionCreation> {
  consumer: number;
  createdAt: string;
  electricity: number;
  gas: number;
  id: number;
  updatedAt: string;
  water: number;
}
