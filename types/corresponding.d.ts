import type { Model } from 'sequelize'
import type dayjs from 'dayjs'

export interface CorrespondingCreation {
  electricity: number;
  endAt: null;
  gas: number;
  waterCO2: number;
  waterElec: number;
}

export interface Corresponding extends Model<Corresponding, CorrespondingCreation> {
  createdAt: string;
  electricity: number;
  endAt: string | null;
  gas: number;
  id: number;
  waterCO2: number;
  waterElec: number;
}

export interface WorkCorresponding extends Omit<Corresponding, 'createdAt' | 'endAt'> {
  createdAt: dayjs.Dayjs;
  endAt: dayjs.Dayjs | null;
}
