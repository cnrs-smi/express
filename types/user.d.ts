import type { Model } from 'sequelize'

export interface UserCreation {
  email: string;
  emailConfirmationToken: string;
  emailConfirmed: boolean;
  isAdmin: boolean;
  password: string;
  peopleNumber: number;
  primaryResidence: string;
  surface: number;
}

export interface User extends Model<User, UserCreation> {
  CodeINSEE: string;
  createdAt: string;
  email: string;
  emailConfirmationToken: string | null;
  emailConfirmed: boolean;
  id: number;
  isAdmin: boolean;
  password?: string;
  peopleNumber: number;
  primaryResidence: string;
  surface: number;
  updatedAt: string;
}
