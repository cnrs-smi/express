export interface JWTBody {
  email: string;
  exp: number;
  iat: number;
  id: number;
}

export interface ListMeta {
  count?: number;
  limit?: number;
  page?: number;
}
