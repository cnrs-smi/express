module.exports = {
    apps: [
        {
            name: 'smi-api',
            script: 'npm',
            args: 'run dev',
        },
    ],
}
